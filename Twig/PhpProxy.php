<?php

namespace phimath\DesignBundle\Twig;


class PhpProxy
{
    public function __call($func, $args)
    {
        if (function_exists($func)) {
            return $func(...$args);
        } else {
            return null;
        }
    }
}