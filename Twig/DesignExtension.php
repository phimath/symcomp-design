<?php

namespace phimath\DesignBundle\Twig;

class DesignExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{

    public function __construct()
    {
    }

    public function getGlobals()
    {
        return [
            'PHP' => new PhpProxy(),
        ];
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('explode', [$this, 'explodeFilter']),
            new \Twig_SimpleFilter('stripos', [$this, 'striposFilter']),
            new \Twig_SimpleFilter('var_dump', [$this, 'var_dumpFilter']),
            new \Twig_SimpleFilter('substr', [$this, 'substrFilter']),
        ];
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('asort', [$this, 'asort']),
            new \Twig_SimpleFunction('exec', [$this, 'exec']),
            new \Twig_SimpleFunction('getenv', [$this, 'getenv']),
        ];
    }

    public function explodeFilter($input, $delimiter)
    {
        return explode($delimiter, $input);
    }

    public function striposFilter($input, $needle)
    {
        return stripos($input, $needle);
    }

    public function substrFilter($string, $start, $length)
    {
        return substr($string, $start, $length);
    }

    public function var_dumpFilter($input)
    {
        return var_dump($input);
    }

    public function asort($array, $sort_flags = null)
    {
        return asort($array, $sort_flags);
    }

    public function exec($callback, ...$params)
    {
        return call_user_func_array($callback, $params);
    }

    public function getenv($arg)
    {
        return getenv($arg);
    }
}