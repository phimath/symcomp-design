<?php

namespace phimath\DesignBundle\Event;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Routing\Router;

class MaintenanceListener
{
    public const NO_MAINTENANCE_ON = [
        'phimath.confluence.document.view',
        // TODO: Add quicklink paths and add here
        'phimath.quicklink.'
    ];
    protected $router;
    protected $container;
    protected $env;

    public function __construct(Router $router, ContainerInterface $container, $env)
    {
        $this->router = $router;
        $this->container = $container;
        $this->env = $env;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $maintenanceRoute = 'phimath.design.maintenance';
        $redirectUrl = $this->router->generate($maintenanceRoute);
        $homeUrl = $this->router->generate('phimath.design.content.index');

        $route = $event->getRequest()->get('_route');
        $maintenanceSiteRequested = $route == $maintenanceRoute;
        $envIsDev = $this->env == "dev";
        $siteEnabled = !$this->container->hasParameter('enabled') || $this->container->getParameter('enabled') == true;

        // Don't handle for documents at all
        if (in_array($route, MaintenanceListener::NO_MAINTENANCE_ON)) {
            return;
        } // If user still has the maintenance page but it's gone by now, redirect the user on refresh
        elseif ($maintenanceSiteRequested && $siteEnabled && !$envIsDev) {
            $event->setController(function () use ($homeUrl) {
                return new RedirectResponse($homeUrl);
            });
        } // If the maintenance site is requested (and previous is not fullfilled) do not redirect
        elseif ($maintenanceSiteRequested || $envIsDev) {
            return;
        } // If the site is disabled, redirect to maintenance site on prod
        elseif (!$envIsDev && !$siteEnabled) {
            $event->setController(function () use ($redirectUrl) {
                return new RedirectResponse($redirectUrl);
            });
        }
    }
}