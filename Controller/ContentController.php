<?php

namespace phimath\DesignBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use phimath\BlogBundle\Service;

class ContentController extends Controller
{
    public function indexAction()
    {
        return $this->render('DesignBundle::index.html.twig');
    }

    public function imprintAction()
    {
        return $this->render('DesignBundle::imprint.html.twig');
    }

    public function welcomeAction()
    {
        // replace this example code with whatever you need
        return $this->render('DesignBundle::welcome.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
        ]);
    }

    public function maintenanceAction()
    {
        return $this->render('DesignBundle::maintenance.html.twig', [], new Response('', Response::HTTP_SERVICE_UNAVAILABLE));
    }
}
